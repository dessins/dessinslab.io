.. nodoctest

Constellation Generation
========================

.. automodule:: dessins.constellations.generation
   :members:
   :undoc-members:
   :show-inheritance: