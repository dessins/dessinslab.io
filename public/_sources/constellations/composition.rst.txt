.. nodoctest

Constellation Composition
=========================

.. automodule:: dessins.constellations.composition
   :members:
   :undoc-members:
   :show-inheritance: